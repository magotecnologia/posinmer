package com.magotecnologia.posinmer.domain;

import com.magotecnologia.posinmer.data.entities.RoleDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 3/10/2018
 * Proyecto: PosInMer
 */
public class Role {
    private String name;
    private List<String> grants;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getGrants() {
        return grants;
    }

    public void setGrants(List<String> grants) {
        this.grants = grants;
    }

    public Role(RoleDTO roleFromData) {
        this.name=roleFromData.getName();
        grants= new ArrayList<>();
        roleFromData.getGrants().stream().forEach(grand->grants.add(grand.getName()));
    }
}
