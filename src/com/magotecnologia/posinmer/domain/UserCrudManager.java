package com.magotecnologia.posinmer.domain;

import com.magotecnologia.posinmer.data.SQLUsersRepository;
import com.magotecnologia.posinmer.data.UsersDataRepository;

import java.util.List;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 22/10/2018
 * Proyecto: PosInMer
 */
public class UserCrudManager {
    private UsersDataRepository repository;

    public UserCrudManager() {
        this.repository= new SQLUsersRepository();
    }

    public List<User>getAllUsers(){
        return repository.findAllUsers();
    }

    public List<User> getUserByWord(String word){
        return repository.findUserByName(word);
    }

}
