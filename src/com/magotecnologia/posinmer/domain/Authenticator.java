package com.magotecnologia.posinmer.domain;

import com.magotecnologia.posinmer.exception.NotFoundUser;
import com.magotecnologia.posinmer.exception.RoleWithoutGrants;
import com.magotecnologia.posinmer.exception.UserWithoutRole;
import com.magotecnologia.posinmer.data.UsersDataRepository;
import com.magotecnologia.posinmer.data.SQLUsersRepository;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 3/10/2018
 * Proyecto: PosInMer
 */
public class Authenticator {

    UsersDataRepository repository;
    User authenticatedUser;

    public User getAuthenticatedUser() {
        return authenticatedUser;
    }

    public void setAuthenticatedUser(User authenticatedUser) {
        this.authenticatedUser = authenticatedUser;
    }

    public boolean authenticate(String user, String pass) throws NotFoundUser, UserWithoutRole, RoleWithoutGrants {
        try {
            User foundUser= repository.getUser(user);
            if(foundUser.getRole()==null){
                throw new UserWithoutRole();
            }
            if(foundUser.getRole().getGrants().isEmpty()) {
                throw new RoleWithoutGrants();
            }
            if(foundUser.getPassword().equals(getSha256(pass))){
                authenticatedUser=foundUser;
                return true;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NotFoundUser notFoundUser) {
            throw notFoundUser;
        }
        return false;
    }
    private String getSha256(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(text.getBytes("UTF-8"));
        byte[] hash = digest.digest();
        String result = DatatypeConverter.printHexBinary(hash);
        return result;
    }

    public Authenticator() {
        repository=new SQLUsersRepository();
    }
}
