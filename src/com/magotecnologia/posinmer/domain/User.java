package com.magotecnologia.posinmer.domain;

import com.magotecnologia.posinmer.data.entities.UserDTO;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 3/10/2018
 * Proyecto: PosInMer
 */
public class User {

    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String documentNumber;
    private boolean active;
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public User(UserDTO dataUser) {
        this.active=dataUser.isActive();
        this.documentNumber=dataUser.getDocumentNumber();
        this.firstName=dataUser.getFirstName();
        this.lastName=dataUser.getUserName();
        this.password=dataUser.getPassword();
        this.role= new Role(dataUser.getRole());
    }
    
    public boolean checkGrant(String grantName){
        foreach(String grant in role.grants){
            if(grant.equals(grantName)){
            return true;}
        }
        return false;
    }
}

