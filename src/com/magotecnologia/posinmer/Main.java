package com.magotecnologia.posinmer;

import com.magotecnologia.posinmer.view.FXMLView;
import com.magotecnologia.posinmer.view.StageManager;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 11/10/2018
 * Proyecto: PosInMer
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        StageManager stageManager=new StageManager(primaryStage);
        stageManager.setLocation("es");
        stageManager.switchScene(FXMLView.LOGIN);
    }


    public static void main(String[] args) {
        launch(args);
    }
}

