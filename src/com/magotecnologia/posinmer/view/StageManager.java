package com.magotecnologia.posinmer.view;

import com.magotecnologia.posinmer.domain.User;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 11/10/2018
 * Proyecto: PosInMer
 */
public class StageManager {

    private Locale location;
    private Stage primaryStage;
    private ResourceBundle languageResources;
    private User session;

    public User getSession() {
        return session;
    }

    public void setSession(User session) {
        this.session = session;
    }

    public Locale getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = new Locale(location,"US");
        Locale.setDefault(this.location);
        this.languageResources=ResourceBundle.getBundle("com.magotecnologia.posinmer.resources.string");
    }

    public StageManager(Stage stage) {
        this.primaryStage=stage;
    }

    public void switchScene(FXMLView view){
        Parent root = null;
        try {
            FXMLLoader loader=new FXMLLoader(getClass().getResource(view.getFxmlFile()));
            loader.setResources(languageResources);
            loader.setController(view.getController(this));
            primaryStage.setTitle(languageResources.getString("AppName"));
            root=loader.load();
            showScene(root,view);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showScene(final Parent root,final FXMLView view){
        Scene scene=new Scene(root, view.getWidth(), view.getHeight());
        scene.setRoot(root);
        scene.getStylesheets().add(getClass().getClassLoader().getResource("com/magotecnologia/posinmer/view/login.css").toString());
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

}
