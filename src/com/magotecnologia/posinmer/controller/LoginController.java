package com.magotecnologia.posinmer.controller;

import com.magotecnologia.posinmer.exception.*;
import com.magotecnologia.posinmer.domain.Authenticator;
import com.magotecnologia.posinmer.view.FXMLView;
import com.magotecnologia.posinmer.view.StageManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.persistence.PersistenceException;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 11/10/2018
 * Proyecto: PosInMer
 */
public class LoginController {

    private final StageManager stageManager;

    @FXML
    public TextField txt_username;

    @FXML
    public TextField txt_password;

    public LoginController(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    private void authenticate(){
        try{
            Authenticator authentication= new Authenticator();
            authentication.authenticate(txt_username.getText().trim(),txt_password.getText().trim());
            stageManager.setSession(authentication.getAuthenticatedUser());
            stageManager.switchScene(FXMLView.MAIN);
        }
        catch (NotFoundUser notFoundUser) {
            postErrorMessage("usuario");
        } catch (UserWithoutRole userWithoutRole) {
            userWithoutRole.printStackTrace();
        } catch (RoleWithoutGrants roleWithoutGrants) {
            roleWithoutGrants.printStackTrace();
        }
        catch (PersistenceException dbException){
            postErrorMessage(dbException.getCause().getMessage());
        }

    }

    @FXML
    private void validateFields(ActionEvent action){
        try {
            removeBlankMessage(txt_username);
            removeBlankMessage(txt_password);
            validateNullableUser(txt_username.getText().trim());
            validateNullablePassword(txt_password.getText().trim());
            authenticate();
        }
        catch (NullUserName nullUser){
            blankMessage(txt_username);
        }
        catch (NullPassword nullPassword) {
            blankMessage(txt_password);
        }
    }

    private void validateNullableUser(String userName) throws NullUserName {
        if(userName.equals(null)||userName.isEmpty()){
            throw new NullUserName();
        }
    }

    private void validateNullablePassword(String password) throws NullPassword {
        if(password.equals(null)||password.isEmpty()){
            throw new NullPassword();
        }
    }

    private void postErrorMessage(String text){
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        Text textToPost= new Text(text);
        Button confirmButton=new Button("Ok");
        confirmButton.setOnAction(actionEvent->dialogStage.close());
        VBox vbox = new VBox(textToPost,confirmButton );
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(15));
        dialogStage.setScene(new Scene(vbox));
        dialogStage.show();
    }

    private void blankMessage(TextField blankText){
        blankText.getStyleClass().add("error");
    }

    private void removeBlankMessage(TextField blankText){
        blankText.getStyleClass().remove("error");
    }

}

