package com.magotecnologia.posinmer.controller;

import com.magotecnologia.posinmer.domain.User;
import com.magotecnologia.posinmer.view.StageManager;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 10/10/2018
 * Proyecto: PosInMer
 */
public class MainController {

    private final StageManager stageManager;

    @FXML
    private AnchorPane usersContent;


    public MainController(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @FXML private UsersTabController usersContentController;
    @FXML private RolesTabController rolesContentController;

    @FXML private void initialize() {
        usersContentController.injectMainController(this);
        User session=stageManager.getSession();
        session.setActive(true);
        stageManager.setSession(session);
    }


}
