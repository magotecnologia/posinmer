package com.magotecnologia.posinmer.controller;

import com.magotecnologia.posinmer.domain.User;
import com.magotecnologia.posinmer.domain.UserCrudManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 10/10/2018
 * Proyecto: PosInMer
 */
public class UsersTabController {
    private MainController mainController;
    private ObservableList<User> users;
    private UserCrudManager crudManager;
    @FXML private TableView<User> usersList;
    @FXML private TextField searchBar;
    @FXML private TableColumn <User, String> tableUserName=new TableColumn();;




    public void injectMainController(MainController mainController) {

        this.mainController=mainController;
        crudManager= new UserCrudManager();
        users= FXCollections.observableArrayList();
        setTableStructure();
    }



    private void setTableStructure(){
        //tableUserName.setCellValueFactory(new PropertyValueFactory<>("userName"));
        tableUserName.setCellValueFactory(new PropertyValueFactory<User,String>("firstName"));
    }



    public void searchField(KeyEvent keyEvent) {
        if(searchBar.getText().trim().isEmpty()){
            updateList(crudManager.getAllUsers());
        }
        else {
            updateList(crudManager.getUserByWord(searchBar.getText().trim()));           
        }
    }
    
    private void updateList(List<User> newList){
            users.clear();
            users.addAll(newList);
            usersList.setItems(users);
    }
}