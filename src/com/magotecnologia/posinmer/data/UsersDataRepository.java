package com.magotecnologia.posinmer.data;

import com.magotecnologia.posinmer.exception.NotFoundUser;
import com.magotecnologia.posinmer.domain.User;

import java.util.List;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 3/10/2018
 * Proyecto: PosInMer
 */
public interface UsersDataRepository {

    User getUser(String username) throws NotFoundUser;
    void saveUser(User newUser);
    void modifyUser(User modifiedUser);
    List<User> findUserByName(String name);
    List<User> findAllUsers();
}
