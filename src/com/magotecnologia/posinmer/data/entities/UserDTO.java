package com.magotecnologia.posinmer.data.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 3/10/2018
 * Proyecto: PosInMer
 */
@Entity
@Table(name = "UserList")
@NamedQueries({

        @NamedQuery(name = "User.findByUserName", query = "SELECT c FROM UserDTO c WHERE c.userName = :userName"),
        @NamedQuery(name = "User.findByUserNameContains", query = "SELECT c FROM UserDTO c WHERE c.userName like :userName"),
        @NamedQuery(name = "User.findById", query = "SELECT c FROM UserDTO c WHERE c.id = :id"),
        @NamedQuery(name = "User.findAll", query = "SELECT c FROM UserDTO c")
       }
)

public class UserDTO  implements Serializable {
    private int id;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String documentNumber;
    private boolean active;
    private RoleDTO role;

    @ManyToOne(cascade = CascadeType.MERGE)
    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public UserDTO() {
    }
    @Column(unique = true,name="Name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserDTO(String userName, String password, String firstName, String lastName, String documentNumber, boolean active, RoleDTO role) {
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.documentNumber = documentNumber;
        this.active = active;
        this.role=role;
    }
}
