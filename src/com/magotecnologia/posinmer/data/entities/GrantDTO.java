package com.magotecnologia.posinmer.data.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 4/10/2018
 * Proyecto: PosInMer
 */
@Entity
@Table(name = "Grants")
public class GrantDTO implements Serializable {
    private String name;
    private String description;

    @Id
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GrantDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public GrantDTO() {
    }
}
