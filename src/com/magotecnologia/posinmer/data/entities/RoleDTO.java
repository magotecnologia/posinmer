package com.magotecnologia.posinmer.data.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 3/10/2018
 * Proyecto: PosInMer
 */
@Entity
@Table(name = "Roles")
@NamedQueries({

        @NamedQuery(name = "Role.findByName", query = "SELECT c FROM RoleDTO c WHERE c.name = :name"),
}
)
public class RoleDTO implements Serializable {
    private String name;
    private List<GrantDTO> grants;
    @Id
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(cascade = CascadeType.MERGE)
    public List<GrantDTO> getGrants() {
        return grants;
    }

    public void setGrants(List<GrantDTO> grants) {
        this.grants = grants;
    }

}