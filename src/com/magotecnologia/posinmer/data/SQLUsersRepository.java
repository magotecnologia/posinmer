package com.magotecnologia.posinmer.data;

import com.magotecnologia.posinmer.exception.NotFoundUser;
import com.magotecnologia.posinmer.domain.User;
import com.magotecnologia.posinmer.data.entities.GrantDTO;
import com.magotecnologia.posinmer.data.entities.RoleDTO;
import com.magotecnologia.posinmer.data.entities.UserDTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

/**
 * Creado por IntelliJ IDEA
 * Usuario: Marco-PC
 * Fecha: 3/10/2018
 * Proyecto: PosInMer
 */
public class SQLUsersRepository implements UsersDataRepository {


    @Override
    public List<User> findUserByName(String name) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnit");
        EntityManager em = emf.createEntityManager();
        TypedQuery<UserDTO> consultaAlumnos= em.createNamedQuery("User.findByUserNameContains", UserDTO.class);
        consultaAlumnos.setParameter("userName", "%"+name+"%");
        List<User> lista= new ArrayList<>();
        consultaAlumnos.getResultList().stream().forEach(x->lista.add(new User(x)));
        return lista;
    }

    @Override
    public List<User> findAllUsers() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnit");
        EntityManager em = emf.createEntityManager();
        TypedQuery<UserDTO> consultaAlumnos= em.createNamedQuery("User.findAll", UserDTO.class);
        List<User> lista= new ArrayList<>();
        consultaAlumnos.getResultList().stream().forEach(x->lista.add(new User(x)));
        return lista;
    }

    @Override
    public User getUser(String username) throws NotFoundUser {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnit");
        EntityManager em = emf.createEntityManager();
        TypedQuery<UserDTO> consultaAlumnos= em.createNamedQuery("User.findByUserName", UserDTO.class);
        consultaAlumnos.setParameter("userName", username);
        List<UserDTO> lista= consultaAlumnos.getResultList();
        UserDTO foundUserData=lista.stream().findAny().orElseThrow(NotFoundUser::new);
        return new User(foundUserData);
    }


    public void saveUser(String hashedPass){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnit");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        RoleDTO admin=new RoleDTO();
        admin.setName("Administrador");
        List<GrantDTO> grants=new ArrayList<>();
        grants.add(new GrantDTO("vender","permiso que le deja vender"));
        grants.add(new GrantDTO("adicionar producto","permiso que le añadir un nuevo producto"));
        admin.setGrants(grants);
        UserDTO userToSave=new UserDTO("test1", hashedPass,"marco","gonzalez","1031122293",true,admin);
        em.persist(userToSave);
        em.getTransaction().commit();
        em.close();
        //em.flush();
    }

    @Override
    public void saveUser(User newUser) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnit");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(newUser);
        em.getTransaction().commit();
        em.close();
    }


    @Override
    public void modifyUser(User modifiedUser) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnit");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(modifiedUser);
        em.getTransaction().commit();
        em.close();
    }
}
